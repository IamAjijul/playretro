package com.datacellme.playretro;

public class Change {
    String subject;

    public Change(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }
}
