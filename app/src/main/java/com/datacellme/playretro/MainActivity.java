package com.datacellme.playretro;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.saifzone.saifviolations.retrofit.ApiServices;
import com.saifzone.saifviolations.retrofit.RequestHandler;
import com.saifzone.saifviolations.retrofit.RetroInstance;
import com.saifzone.saifviolations.retrofit.SetOnResponseListener;

import org.jetbrains.annotations.Nullable;
import org.json.JSONObject;

import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ApiServices apiServices = RetroInstance.Companion.getInstance().create(ApiServices.class);
        apiServices.getUsers().enqueue(new RequestHandler <String>(new SetOnResponseListener() {
            @Override
            public <T> void onSuccess(@Nullable Response<T> response) {
//                ValueModel valueModel = (ValueModel)response.body();
                Log.e("##","********"+response.body());
            }

            @Override
            public void onError(@Nullable String error) {

            }
        },this));





//        Controller controller = new Controller();
//        controller.start();
//
//        List<Change> list = new ArrayList<Change>();
//        for (int i = 0; i < 20; i++) {
//            list.add(new Change("Test "+i));
//        }
//        Log.e("###",""+list.toString());
//
//        Gson gson = new Gson();
//        Type type = new TypeToken<List<Change>>() {}.getType();
//        String json = gson.toJson(list, type);
//        Log.e("###",""+json);
//
//        System.out.println(json);
//        List<Change> fromJson = gson.fromJson(json, type);
//
//        for (Change task : fromJson) {
//            System.out.println(task);
//        }
    }
}
