package com.datacellme.playretro;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class ValueModel implements Parcelable {
    int page, per_page, total, total_pages;
    ArrayList<UserModel> data;

    public ValueModel(int page, int per_page, int total, int total_pages, ArrayList<UserModel> data) {
        this.page = page;
        this.per_page = per_page;
        this.total = total;
        this.total_pages = total_pages;
        this.data = data;
    }

    public int getPage() {
        return page;
    }

    public int getPer_page() {
        return per_page;
    }

    public int getTotal() {
        return total;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public ArrayList<UserModel> getData() {
        return data;
    }

    protected ValueModel(Parcel in) {
        page = in.readInt();
        per_page = in.readInt();
        total = in.readInt();
        total_pages = in.readInt();
        data = in.createTypedArrayList(UserModel.CREATOR);
    }

    public static final Creator<ValueModel> CREATOR = new Creator<ValueModel>() {
        @Override
        public ValueModel createFromParcel(Parcel in) {
            return new ValueModel(in);
        }

        @Override
        public ValueModel[] newArray(int size) {
            return new ValueModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}

