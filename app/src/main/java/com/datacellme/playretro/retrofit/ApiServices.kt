package com.saifzone.saifviolations.retrofit

import com.datacellme.playretro.UserModel
import com.datacellme.playretro.ValueModel
import com.google.gson.JsonObject
import org.json.JSONObject
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

 interface ApiServices {

    @GET("users")
    fun getUsers() : Call<String>


}