package com.saifzone.saifviolations.retrofit

import com.datacellme.playretro.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import java.util.concurrent.TimeUnit

class RetroInstance private constructor() {

    companion object {
        private var mInstance: Retrofit? = null
        private val BASE_URL: String = "https://reqres.in/api/"
        private val mRequestTimeOut: Long = 180
        private val mReadTimeOut: Long = 180
        private val mWriteTimeOut: Long = 180

        @Synchronized
        fun getInstance(): Retrofit {
            if (mInstance == null) {
                mInstance = Retrofit.Builder().baseUrl(BASE_URL).client(getNetworkClient()
                ).build()
            }
            return mInstance!!
        }

        private fun getNetworkClient(): OkHttpClient {
            val builder: OkHttpClient.Builder = OkHttpClient().newBuilder()
                    .connectTimeout(mRequestTimeOut, TimeUnit.SECONDS)
                    .readTimeout(mReadTimeOut, TimeUnit.SECONDS)
                    .writeTimeout(mWriteTimeOut, TimeUnit.SECONDS)
            if (BuildConfig.DEBUG)
                builder.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))

            return builder.build()
        }
    }

}