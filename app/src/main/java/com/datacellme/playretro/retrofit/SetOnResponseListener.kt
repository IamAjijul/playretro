package com.saifzone.saifviolations.retrofit

import retrofit2.Response

interface SetOnResponseListener {
   fun  <T> onSuccess(response: Response<T>?)
    fun onError(error: String?)
}