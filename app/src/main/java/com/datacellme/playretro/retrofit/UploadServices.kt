package com.saifzone.saifviolations.retrofit

import com.google.gson.JsonObject
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part
import retrofit2.http.PartMap

interface UploadServices {
    @Multipart
    @POST
    fun uploadCompanyViolations(@PartMap parameter : Map<String,RequestBody>, @Part images : Array<MultipartBody.Part?>) : Call<JsonObject>

    @Multipart
    @POST
    fun uploadTrafficViolations(@PartMap parameter : Map<String,RequestBody>, @Part images : Array<MultipartBody.Part?>) : Call<JsonObject>

}